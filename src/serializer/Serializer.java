package serializer;

import java.lang.reflect.Field;
import java.util.*;

public class Serializer<T> {
	public Map<String, Object> serialize(T o) {
		return getFieldsMap(o);
	}

	private Map<String, Object> getFieldsMap(T o) {
		Map<String, Object> map = new HashMap<>();
		List<Field> fields = getFields(o);
		for (Field f : fields) {
			f.setAccessible(true);
			try {
				String fk = f.getName();
				Object fv = f.get(o);
				Class<?> fc = Class.forName(f.getType().getName());
				map.put(fk, serialize(fc, fv));
			} catch (IllegalAccessException | ClassNotFoundException e) {
				e.printStackTrace();
			}
		}
		return map;
	}

	private List<Field> getFields(T o) {
		List<Field> fields = new ArrayList<>();
		Class<?> c = o.getClass();
		while (c != Object.class) {
			fields.addAll(getInterfacesFields(c));
			fields.addAll(Arrays.asList(c.getDeclaredFields()));
			c = c.getSuperclass();
		}
		return fields;
	}

	private List<Field> getInterfacesFields(Class<?> c) {
		List<Field> fields = new ArrayList<>();
		for (Class<?> i : c.getInterfaces()) {
			fields.addAll(getInterfacesFields(i));
			fields.addAll(Arrays.asList(i.getDeclaredFields()));
		}
		return fields;
	}

	private Object serialize(Class<?> c, Object o) {
		if (isPrimitive(c)) {
			return serializePrimitive(o);
		} else if (isCollection(c)) {
			return serializeCollection(o);
		} else if (isMap(c)) {
			return serializeMap(o);
		} else {
			return new Serializer<>().serialize(o);
		}
	}

	private String serializePrimitive(Object o) {
		return o.toString();
	}

	private List<Object> serializeCollection(Object o) {
		List<Object> result = new ArrayList<>();
		Collection<?> collection = (Collection<?>) o;
		for (Object object : collection) {
			result.add(serialize(object.getClass(), object));
		}
		return result;
	}

	private Map<Object, Object> serializeMap(Object o) {
		Map<Object, Object> result = new HashMap<>();
		Map<?, ?> map = (Map<?, ?>) o;
		Set<?> keys = map.keySet();
		for (Object key : keys) {
			result.put(
					serialize(key.getClass(), key),
					serialize(map.get(key).getClass(), map.get(key))
			);
		}
		return result;
	}

	private boolean isPrimitive(Class<?> c) {
		return Number.class.isAssignableFrom(c) || String.class.equals(c);
	}

	private boolean isCollection(Class<?> c) {
		return Collection.class.isAssignableFrom(c);
	}

	private boolean isMap(Class<?> c) {
		return Map.class.isAssignableFrom(c);
	}
}