package serializer.example;

import java.util.Map;

public class Student extends Person implements Assessable {
	private String nickname;

	public Student(String name, String nickname) {
		super(name);
		this.nickname = nickname;
	}

	public void addRecord(String subject, int grade) {
		this.recordBook.put(subject, grade);
	}

	public Map<String, Integer> getRecordBook() {
		return recordBook;
	}

	public String getCertificate() {
		return this.certificate;
	}

	public String getNickname() {
		return nickname;
	}

	public void setNickname(String nickname) {
		this.nickname = nickname;
	}

	@Override
	public String toString() {
		return "Student" + '{' +
				"name=" + this.name + ", " +
				"nickname=" + this.nickname + ", " +
				"recordBook=" + this.recordBook + ", " +
				"certificate=" + this.certificate + '}';
	}
}