package serializer.example;

import java.util.HashMap;
import java.util.Map;

public interface Assessable extends Certifiable {
	Map<String, Integer> recordBook = new HashMap<>();

	void addRecord(String subject, int grade);

	Map<String, Integer> getRecordBook();
}