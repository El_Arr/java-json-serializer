package serializer.example;

public interface Certifiable {
	String certificate = "Moordale Secondary School";

	String getCertificate();
}