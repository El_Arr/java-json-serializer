package serializer.example;

import java.util.ArrayList;
import java.util.List;

public class Group {
	private final List<Student> students = new ArrayList<>();

	public List<Student> getStudents() {
		return students;
	}

	@Override
	public String toString() {
		return "Group" + '{' + "students=" + students + '}';
	}
}