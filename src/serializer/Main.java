package serializer;

import serializer.example.Group;
import serializer.example.Student;

public class Main {

	public static void main(String[] args) {
		Student s1 = new Student("Otis", "New kid");
		s1.addRecord("Math", 5);
		s1.addRecord("History", 5);
		System.out.println(s1);

		Student s2 = new Student("Eric", "Tromboner");
		s2.addRecord("Math", 4);
		s2.addRecord("History", 5);
		System.out.println(s2);

		Group g1 = new Group();
		g1.getStudents().add(s1);
		g1.getStudents().add(s2);
		System.out.println(g1);

		DB<Student> dbs = new DB<>("students.json");
		dbs.save(s1);
		dbs.save(s2);

		DB<Group> dbg = new DB<>("groups.json");
		dbg.save(g1);
	}
}