package serializer;

import java.util.Collection;
import java.util.Map;
import java.util.Set;

public class JSON {
	public static String parse(Map<String, Object> map) {
		StringBuilder builder = new StringBuilder();
		builder.append('{').append('\n');
		Set<String> keys = map.keySet();
		for (String key : keys) {
			Object object = map.get(key);
			builder.append(parse(key, object.getClass(), object));
		}
		builder.append('}');
		return builder.toString();
	}

	private static String parse(String k, Class<?> c, Object o) {
		if (isPrimitive(c)) {
			return parsePrimitive(k, o);
		} else if (isCollection(c)) {
			return parseCollection(k, o);
		} else if (isMap(c)) {
			return parseMap(k, o);
		} else {
			return String.format("\"%s\": \"%s\",\n", k, o.toString());
		}
	}

	private static String parsePrimitive(String k, Object o) {
		return String.format("\"%s\": \"%s\",\n", k, o.toString());
	}

	private static String parseCollection(String k, Object o) {
		StringBuilder builder = new StringBuilder();
		builder.append(String.format("\"%s\": ", k));
		builder.append('[').append('\n');
		Collection<?> collection = (Collection<?>) o;
		for (Object object : collection) {
			builder.append(parse(k, object.getClass(), object));
		}
		builder.append(']').append('\n');
		return builder.toString();
	}

	private static String parseMap(String k, Object o) {
		StringBuilder builder = new StringBuilder();
		builder.append(String.format("\"%s\": ", k));
		builder.append('{').append('\n');
		Map<?, ?> map = (Map<?, ?>) o;
		Set<?> keys = map.keySet();
		for (Object key : keys) {
			builder.append(parse(
					key.toString(),
					map.get(key).getClass(),
					map.get(key)
			));
		}
		builder.append('}').append('\n');
		return builder.toString();
	}

	private static boolean isPrimitive(Class<?> c) {
		return Number.class.isAssignableFrom(c) || String.class.equals(c);
	}

	private static boolean isCollection(Class<?> c) {
		return Collection.class.isAssignableFrom(c);
	}

	private static boolean isMap(Class<?> c) {
		return Map.class.isAssignableFrom(c);
	}
}
