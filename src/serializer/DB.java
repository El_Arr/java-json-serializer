package serializer;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

public class DB<T> {
	private final String url;
	private final Serializer<T> serializer;

	public DB(String url) {
		this.url = url;
		this.serializer = new Serializer<>();
	}

	public void save(T object) {
		String serialized = JSON.parse(this.serializer.serialize(object));
		try (PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter(url, true)))) {
			out.println(serialized);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}